require 'rails_helper'

RSpec.describe Spree::OptionValue, type: :model do
  describe "filter_option method" do
    let(:main_taxon) { create(:taxon) }
    let(:dummy_taxon) { create(:taxon) }
    let(:main_filter) { "color" }

    context "when option_types are found" do
      it "returns the correct option_value_name and products_count" do
        expected_option_value_name = 'Red'
        expected_option_values_count = 1
        create(:decorated_product, :product_with_red_color_option, taxons: [main_taxon])
        expect(Spree::OptionValue.filter_option(main_filter, main_taxon).first.first).to eq expected_option_value_name
        expect(Spree::OptionValue.filter_option(main_filter, main_taxon).first.last).to eq expected_option_values_count
      end

      it "returns the correct size" do
        expected_size = 1
        create(:decorated_product, :product_with_red_color_option, taxons: [main_taxon])
        option_value_yellow_color = create(:decorated_option_value, :option_value_yellow)
        option_type_color_with_yellow = create(:option_type, name: 'dummy-color', presentation: 'Color', option_values: [option_value_yellow_color])
        create(:product, taxons: [dummy_taxon], option_types: [option_type_color_with_yellow])
        expect(Spree::OptionValue.filter_option(main_filter, main_taxon).length).to eq expected_size
      end
    end

    context "when option_types are NOT found" do
      it "does NOT include any" do
        option_value_yellow_color = create(:decorated_option_value, :option_value_yellow)
        option_type_color_with_yellow = create(:option_type, name: 'dummy-color', presentation: 'Color', option_values: [option_value_yellow_color])
        create(:product, taxons: [dummy_taxon], option_types: [option_type_color_with_yellow])
        expect(Spree::OptionValue.filter_option(main_filter, main_taxon)).to be_empty
      end
    end
  end
end
