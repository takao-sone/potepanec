require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }

    context "when the requested category exists the layout of grid" do
      before do
        create_list(:taxonomy, 10)
        create_list(:product, 10) do |product|
          product.taxons << taxon
        end
      end

      it "responds successfully" do
        get :show, params: { id: taxon.id }
        expect(response).to be_success
      end

      it "returns HTTP status 200" do
        get :show, params: { id: taxon.id }
        expect(response).to have_http_status "200"
      end

      it "assigns the requested category to @taxon" do
        get :show, params: { id: taxon.id }
        expect(assigns(:taxon)).to eq taxon
      end

      it "assigns the requested taxonomies to @taxonomies" do
        taxonomies = Spree::Taxonomy.taxonomies
        get :show, params: { id: taxon.id }
        expect(assigns(:taxonomies)).to eq taxonomies
      end

      it "assigns the requested products to @products" do
        grid_layout_products_size = 12
        page = 1
        products = Spree::Product.category_products(taxon:taxon, page:page, products_per_page:grid_layout_products_size, filter:nil, sort:nil)
        get :show, params: { id: taxon.id }
        expect(assigns(:products)).to match_array products
      end

      it "renders the show template" do
        get :show, params: { id: taxon.id }
        expect(response).to render_template("potepan/categories/show")
      end
    end

    context "when the requested category exists the layout of list" do
      before do
        create_list(:product, 10) do |product|
          product.taxons << taxon
        end
      end
      it "assigns the requested products to @products" do
        list_layout_products_size = 4
        page = 1
        products = Spree::Product.category_products(taxon:taxon, page:page, products_per_page:list_layout_products_size, filter:nil, sort:nil)
        get :show, params: { id: taxon.id, layout: 'list' }
        expect(assigns(:products)).to match_array products
      end
    end
  end
end
