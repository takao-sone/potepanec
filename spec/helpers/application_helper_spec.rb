require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title method" do
    before do
      @base_title = "BIGBAG Store"
    end

    context "when arguments are passed" do
      it "returns the title with the page title" do
        expect(helper.full_title("TEST")).to eq "TEST - #{@base_title}"
      end
    end

    context "when NO arguments are passed" do
      it "returns the base title" do
        expect(helper.full_title).to eq @base_title
      end
    end
  end
end
