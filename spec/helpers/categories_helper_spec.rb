require 'rails_helper'

RSpec.describe CategoriesHelper, type: :helper do
  describe "taxon_product_size method" do
    let(:main_taxon) { create(:taxon) }
    let(:unrelated_taxon) { create(:taxon) }

    context "when the children of the taxon does NOT exist" do
      it "returns the correct size" do
        product_size = 2
        unrelated_product_size = 2
        create_list(:product, product_size) do |product|
          product.taxons << main_taxon
        end
        create_list(:product, unrelated_product_size) do |product|
          product.taxons << unrelated_taxon
        end
        expect(taxon_products_size(main_taxon)).to eq product_size
      end
    end

    context "when the children of the taxon exist" do
      it "returns the correct size" do
        total_children_product_size = 4
        child_product_size = 2
        unrelated_product_size = 2
        taxon1 = create(:taxon) do |taxon|
          taxon.parent_id = main_taxon.id
        end
        taxon2 = create(:taxon) do |taxon|
          taxon.parent_id = main_taxon.id
        end
        create_list(:product, child_product_size) do |product|
          product.taxons << taxon1
        end
        create_list(:product, child_product_size) do |product|
          product.taxons << taxon2
        end
        create_list(:product, unrelated_product_size)
        expect(taxon_products_size(main_taxon)).to eq total_children_product_size
      end
    end
  end

  describe "grid_layout? method" do
    context "when the value of the arguments is nil" do
      it "returns true" do
        layout = nil
        expect(grid_layout?(layout)).to be_truthy
      end
    end

    context "when the value of the argument is list" do
      it "returns false" do
        layout = "list"
        expect(grid_layout?(layout)).to be_falsey
      end
    end
  end

  describe "list_layout? method" do
    context "when the value of the arguments is nil" do
      it "returns nil" do
        layout = nil
        expect(list_layout?(layout)).to be_falsey
      end
    end

    context "when the value of the argument is list" do
      it "returns nothing" do
        layout = "list"
        expect(list_layout?(layout)).to be_truthy
      end
    end
  end

  describe "set_filter_box_default_value method" do
    context "when the value of the argument is nil" do
      it "returns string, newest" do
        argument_nil = nil
        expected_value = Spree::Product::ORDER_SELECTORS.first[1]
        expect(set_filter_box_default_value(argument_nil)).to eq expected_value
      end
    end

    context "when the value of the argument is NOT nil" do
      it "returns the same value as the argument" do
        argument_oldest = 'oldest'
        expected_value = 'oldest'
        expect(set_filter_box_default_value(argument_oldest)).to eq expected_value
      end
    end
  end
end
