class Potepan::CategoriesController < ApplicationController
  SAME_CATEGORY_PRODUCTS_LIST = 4
  SAME_CATEGORY_PRODUCTS_GRID = 12
  FILTER_OPTION_TYPE_COLOR = "color"
  FILTER_OPTION_TYPE_SIZE = "size"

  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.taxonomies
    @products = Spree::Product
                  .category_products(taxon: @taxon,
                                     filter: params[:filter],
                                     sort: params[:sort],
                                     page: params[:page],
                                     products_per_page: category_products_per_page)
    @colors = Spree::OptionValue.filter_option(FILTER_OPTION_TYPE_COLOR, @taxon)
    @sizes = Spree::OptionValue.filter_option(FILTER_OPTION_TYPE_SIZE, @taxon)
  end

  private

  def category_products_per_page
    params[:layout] == 'list' ? SAME_CATEGORY_PRODUCTS_LIST : SAME_CATEGORY_PRODUCTS_GRID
  end
end
