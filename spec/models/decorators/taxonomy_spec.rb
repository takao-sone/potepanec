require 'rails_helper'

RSpec.describe Spree::Taxonomy, type: :model do
  describe "when taxonomies method" do

    context "when taxonomies are found" do
      it "returns the correct taxonomies" do
        taxonomy = create(:taxonomy)
        taxonomy2 = create(:taxonomy)
        expect(Spree::Taxonomy.taxonomies).to include(taxonomy, taxonomy2)
      end

      it "returns the correct number of taxonomies" do
        number_of_taxonomies = 10
        create_list(:taxonomy, number_of_taxonomies)
        expect(Spree::Taxonomy.taxonomies.length).to eq number_of_taxonomies
      end

    end

    context "when taxonomies are NOT found" do
      it "does NOT return any taxonomy" do
        expect(Spree::Taxonomy.taxonomies).to be_empty
      end
    end
  end
end
