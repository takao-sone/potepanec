FactoryGirl.define do
  factory :decorated_option_value, parent: :option_value do
    trait :option_value_red do
      name 'Red'
      presentation 'Red'
    end

    trait :option_value_yellow do
      name 'Yellow'
      presentation 'Yellow'
    end
  end
end
