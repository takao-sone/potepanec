require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let(:product1) do
      create(:product) do |product|
        product.taxons << taxon
      end
    end
    let(:products) do
      create(:product, 10) do |product|
        product.taxons << taxon
      end
    end
    let(:similar_products) { Spree::Product.similar_products(product1) }

    context "when the requested product exists" do
      it "responds successfully" do
        get :show, params: { id: product1.id }
        expect(response).to be_success
      end

      it "returns HTTP status 200" do
        get :show, params: { id: product1.id }
        expect(response).to have_http_status "200"
      end

      it "assigns the requested product to @product" do
        get :show, params: { id: product1.id }
        expect(assigns(:product)).to eq product1
      end

      it "assigns the similar products of the requested product to @similar_products" do
        get :show, params: { id: product1.id }
        expect(assigns(:similar_products)).to eq similar_products
      end

      it "renders the show template" do
        get :show, params: { id: product1.id }
        expect(response).to render_template("potepan/products/show")
      end
    end
  end
end
