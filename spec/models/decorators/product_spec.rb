require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "similar_products method" do
    let(:main_product_taxon) { create(:taxon) }
    let(:unrelated_products_taxon) { create(:taxon) }
    let(:main_product) do
      create(:product) do |product|
        product.taxons << main_product_taxon
      end
    end

    context "when the similar products are found" do
      it "returns the correct related products" do
        related_product = create(:product)
        related_product.taxons << main_product_taxon
        expect(Spree::Product.similar_products(main_product)).to include(related_product)
      end

      it "does NOT return unrelated products" do
        unrelated_product = create(:product)
        unrelated_product.taxons << unrelated_products_taxon
        expect(Spree::Product.similar_products(main_product)).to_not include(unrelated_product)
      end

      it "does NOT return itself" do
        expect(Spree::Product.similar_products(main_product)).to_not include(main_product)
      end

      it "limits the number of similar products" do
        max_number_of_similar_products = 4
        create_list(:product, 10) do |product|
          product.taxons << main_product_taxon
        end
        expect(Spree::Product.similar_products(main_product).size).to eq max_number_of_similar_products
      end
    end

    context "when the similar products are NOT found" do
      before do
        create_list(:product, 10) do |product|
          product.taxons << unrelated_products_taxon
        end
      end

      it "does NOT include any" do
        expect(Spree::Product.similar_products(main_product)).to be_empty
      end
    end
  end

  describe "category_products method" do
    let(:main_taxon) { create(:taxon) }
    let(:unrelated_taxon) { create(:taxon) }
    let(:first_page) { 1 }
    let(:products_per_page) { 12 }

    context "when the category products are found" do
      it "returns the correct category products" do
        category_product = create(:product)
        category_product.taxons << main_taxon
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, sort:nil, filter:nil)).to include(category_product)
      end

      it "does NOT return unrelated category products" do
        category_product = create(:product)
        category_product.taxons << main_taxon
        unrelated_category_product = create(:product)
        unrelated_category_product.taxons << unrelated_taxon
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, sort:nil, filter:nil)).to_not include(unrelated_category_product)
      end

      it "returns the correct number of the category products" do
        second_page = 2
        max_category_products_size = 12
        second_page_products = 2
        total_correct_category_products = 14
        create_list(:product, total_correct_category_products) do |product|
          product.taxons << main_taxon
        end
        create_list(:product, 10) do |product|
          product.taxons << unrelated_taxon
        end
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, sort:nil, filter:nil).length).to eq max_category_products_size
        expect(Spree::Product.category_products(taxon:main_taxon, page:second_page, products_per_page:products_per_page, sort:nil, filter:nil).length).to eq second_page_products
      end
    end

    context "when the category product WITH red color option is found" do
      it "returns the correct category product" do
        red_color = 'Red'
        product_with_red_color_option = create(:decorated_product, :product_with_red_color_option, taxons: [main_taxon])
        product_without_color_option = create(:product, taxons: [unrelated_taxon])
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:red_color, sort:nil)).to include(product_with_red_color_option)
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:red_color, sort:nil)).not_to include(product_without_color_option)
      end
    end

    context "when the category product WITH yellow color option is NOT found" do
      it "returns the correct category product" do
        yellow_color = 'Yellow'
        create(:decorated_product, :product_with_red_color_option, taxons: [main_taxon])
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:yellow_color, sort:nil)).to be_empty
      end
    end

    context "when category products are sorted by from NEWEST to OLDEST" do
      it "returns category products in correct order" do
        newest_sort = 'newest'
        older_product = create(:decorated_product, :product_available_for_two_years, taxons: [main_taxon])
        newer_product = create(:product, taxons: [main_taxon])
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:newest_sort).first).to eq newer_product
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:newest_sort).last).to eq older_product
      end
    end

    context "when category products are sorted by from OLDEST to NEWEST" do
      it "returns category products in correct order" do
        oldest_sort = 'oldest'
        older_product = create(:decorated_product, :product_available_for_two_years, taxons: [main_taxon])
        newer_product = create(:product, taxons: [main_taxon])
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:oldest_sort).first).to eq older_product
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:oldest_sort).last).to eq newer_product
      end
    end

    context "when category products are sorted by from AFFORDABLE to EXPENSIVE" do
      it "returns category products in correct order" do
        affordable_sort = 'affordable'
        affordable_product = create(:product, price: 1, taxons: [main_taxon])
        more_expensive_product = create(:product, price: 1000000, taxons: [main_taxon])
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:affordable_sort).first).to eq affordable_product
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:affordable_sort).last).to eq more_expensive_product
      end
    end

    context "when category products are sorted by from EXPENSIVE to AFFORDABLE" do
      it "returns category products in correct order" do
        expensive_sort = 'expensive'
        affordable_product = create(:product, price: 1, taxons: [main_taxon])
        more_expensive_product = create(:product, price: 1000000, taxons: [main_taxon])
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:expensive_sort).first).to eq more_expensive_product
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:expensive_sort).last).to eq affordable_product
      end
    end

    context "when the category products are NOT found" do
      it "does NOT include any" do
        expect(Spree::Product.category_products(taxon:main_taxon, page:first_page, products_per_page:products_per_page, filter:nil, sort:nil)).to be_empty
      end
    end
  end

  describe "new_products method" do
    let(:product_available_for_one_year) { create(:product) }
    let(:new_product_released_in_future) { create(:decorated_product, :product_available_in_future) }
    let(:product_available_for_two_years) { create(:decorated_product, :product_available_for_two_years) }

    context "when new products are found" do
      it "returns the correct new products" do
        expect(Spree::Product.new_products).to include(product_available_for_one_year)
      end

      it "does NOT return products available on future date" do
        expect(Spree::Product.new_products).not_to include(new_product_released_in_future)
      end

      it "returns the correct number of new products" do
        max_new_products = 12
        actual_new_products = 14
        create_list(:product, actual_new_products)
        expect(Spree::Product.new_products.size).to eq max_new_products
      end

      it "returns new products in the correct order" do
        expect(Spree::Product.new_products).to match([product_available_for_one_year, product_available_for_two_years])
      end
    end

    context "when new products are not found" do
      it "does NOT return any new products" do
        expect(Spree::Product.new_products).to be_empty
      end
    end
  end
end
