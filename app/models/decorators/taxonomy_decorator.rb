Spree::Taxonomy.class_eval do
  scope :taxonomies, -> {
    includes(root: [children: [:products, children: [:products]]])
  }
end