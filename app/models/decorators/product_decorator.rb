Spree::Product.class_eval do
  MAX_NEW_PRODUCTS = 12
  MAX_SIMILAR_PRODUCTS = 4
  ORDER_SELECTORS = [
    %w(新着順 newest),
    %w(価格の安い順 affordable),
    %w(価格の高い順 expensive),
    %w(古い順 oldest)
  ]
  ORDER_SQL_OPTIONS = [
    { newest: 'spree_products.available_on DESC' },
    { affordable: 'spree_prices.amount ASC' },
    { expensive: 'spree_prices.amount DESC' },
    { oldest: 'spree_products.available_on ASC' }
  ]

  scope :products_with_images_and_prices, -> {
    includes(master: [:images, :default_price])
  }

  scope :new_products, -> {
    products_with_images_and_prices
      .available.order(available_on: :desc)
      .limit(MAX_NEW_PRODUCTS)
  }

  scope :similar_products, -> (product) {
    in_taxons(product.taxon_ids)
      .products_with_images_and_prices
      .where.not(spree_products: { id: product })
      .distinct.limit(MAX_SIMILAR_PRODUCTS)
  }

  scope :category_products, -> (
    taxon:,
      sort:,
      filter: nil,
      page:,
      products_per_page:
  ) {
    sort = ORDER_SELECTORS.first[1] if sort.nil?
    order_detail = ORDER_SQL_OPTIONS
                     .find { |hash| hash.with_indifferent_access.has_key?(sort) }
                     .with_indifferent_access[sort]
    if filter.nil?
      in_taxon(taxon)
        .products_with_images_and_prices
        .available.reorder(order_detail)
        .page(page).per(products_per_page)
    else
      in_taxon(taxon)
        .products_with_images_and_prices
        .includes(option_types: :option_values)
        .where(spree_option_values: { name: filter })
        .available.reorder(order_detail)
        .page(page).per(products_per_page)
    end
  }
end
