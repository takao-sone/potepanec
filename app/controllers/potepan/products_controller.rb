class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @similar_products = Spree::Product.similar_products(@product)
  end
end
