FactoryGirl.define do
  factory :decorated_product, parent: :product do
    trait :product_available_in_future do
      available_on { 100.years.since }
    end

    trait :product_available_for_two_years do
      available_on { 2.years.ago }
    end

    trait :product_with_red_color_option do
      after(:create) do |product|
        create(:decorated_option_type, :option_type_color, products: [product])
      end
    end
  end
end