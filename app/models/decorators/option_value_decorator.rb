Spree::OptionValue.class_eval do
  def self.filter_option(filter, taxon)
    taxon_ids = taxon.self_and_descendants.pluck(:id)
    joins(option_type: { products: :taxons })
      .where('spree_option_types.name LIKE ?', "%#{filter}%")
      .where(spree_taxons: { id: taxon_ids })
      .group("spree_option_values.name")
      .reorder(nil)
      .count
      .to_a
  end
end
