require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "#index" do
    it "responds successfully" do
      get :index
      expect(response).to be_success
    end

    it "returns HTTP status 200" do
      get :index
      expect(response).to have_http_status "200"
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("potepan/home/index")
    end
  end
end
