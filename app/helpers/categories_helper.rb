module CategoriesHelper
  def taxon_products_size(root_taxon)
    size = root_taxon.products.size
    root_taxon.children.each do |child_taxon|
      size += child_taxon.products.size
    end
    size
  end

  def grid_layout?(layout)
    layout.nil?
  end

  def list_layout?(layout)
    layout == 'list'
  end

  def set_filter_box_default_value(sort)
    if sort.nil?
      Spree::Product::ORDER_SELECTORS.first[1]
    else
      sort
    end
  end
end