FactoryGirl.define do
  factory :decorated_option_type, parent: :option_type do
    trait :option_type_color do
      name 'mug-color'
      presentation 'Mug Color'
      after(:create) do |option_type|
        create(:decorated_option_value, :option_value_red, option_type: option_type)
      end
    end
  end
end
